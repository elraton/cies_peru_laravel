<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function () {
    return view('home');
});
Route::get('about', function () {
    return view('about');
});
Route::get('ponents', function () {
    return view('ponents');
});
Route::get('program_thomas', function () {
    return view('program_thomas');
});
Route::get('program_bricot', function () {
    return view('program_bricot');
});
Route::get('program_cecile', function () {
    return view('program_cecile');
});
Route::get('certifications', function () {
    return view('certifications');
});
Route::get('contact', function () {
    return view('contact');
});
Route::post('registro', 'RegistroController@insert');