require('./bootstrap');

window.onload = () => {

    window.scroll({
        top: 0,
        left: 0, 
        behavior: 'smooth'
    });

    var toggleMenu = () => {
        const menu_cont = document.querySelector('.menu_mobile_cont');
        if ( menu_cont.classList.contains('open') ) {
            menu_cont.style.left = '0';
            setTimeout(() => {
                menu_cont.style.left = '100%';
                setTimeout(() => {
                    menu_cont.className = 'menu_mobile_cont close';
                }, 999);
            }, 1);
        } else {
            menu_cont.className = 'menu_mobile_cont open';
            menu_cont.style.left = '100%';
            setTimeout(() => {
                menu_cont.style.left = '0';
            }, 10);
        }
    };

    document.querySelector('.close_menu_button').onclick = (e) => {
        e.preventDefault();
        toggleMenu();
    };

    document.querySelector('.menu_mobile_button').onclick = (e) => {
        e.preventDefault();
        toggleMenu();
    };

    var snackbar = document.querySelector("#snackbar");
    var showSnackbar = (message) => {
        snackbar.innerHTML = message;
        snackbar.className = "show";
        setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    };

    var formsubmit = (e) => {
        e.preventDefault();

        var name = document.getElementById('form_name').value;
        var email = document.getElementById('form_email').value;
        var phone = document.getElementById('form_phone').value;
        var subject = document.getElementById('form_subject').value;
        var message = document.getElementById('form_message').value;

        if ( name.length < 6) {
            showSnackbar('Nombre no valido, muy corto');
            return;
        }
        if ( email.length < 6) {
            showSnackbar('Email no valido');
            return;
        }
        if ( phone.length < 8) {
            showSnackbar('Teléfono no valido');
            return;
        }
        if ( subject.length < 10) {
            showSnackbar('Asunto muy corto');
            return;
        }
        if ( message.length < 10) {
            showSnackbar('Mensaje muy corto');
            return;
        }

        if ( name.length > 100) {
            showSnackbar('Nombre muy largo');
            return;
        }
        if ( email.length > 100) {
            showSnackbar('Email muy largo');
            return;
        }
        if ( phone.length > 9) {
            showSnackbar('Teléfono muy largo');
            return;
        }
        if ( subject.length > 100) {
            showSnackbar('Asunto muy largo');
            return;
        }
        if ( message.length > 500) {
            showSnackbar('Mensaje muy largo');
            return;
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(email).toLowerCase()) ) {
            showSnackbar('Email no valido');
            return;
        }

        if (isNaN(Number(phone))) {
            showSnackbar('Teléfono no valido');
            return;
        }

        var data = {
            name: name,
            email: email,
            phone: phone,
            subject: subject,
            message: message
        };

        fetch('/registro', {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be `string` or {object}!
            headers:{
                'X-CSRF-TOKEN': document.querySelector('input[name=_token]').value,
                'Content-Type': 'application/json'
            }
          }).then( (res) => {
              if (res.status == 200) {
                showSnackbar('Se envio con éxito');
                document.getElementById('form_name').value = '';
                document.getElementById('form_email').value = '';
                document.getElementById('form_phone').value = '';
                document.getElementById('form_subject').value = '';
                document.getElementById('form_message').value = '';
              } else {
                showSnackbar('Ocurrio un error');
              }
            })
          .catch(error => showSnackbar('Ocurrio un error'))
          .then((response) => {
              if (response.status == 200) {
                showSnackbar('Se envio con éxito');
                document.getElementById('form_name').value = '';
                document.getElementById('form_email').value = '';
                document.getElementById('form_phone').value = '';
                document.getElementById('form_subject').value = '';
                document.getElementById('form_message').value = '';
              } else {
                showSnackbar('Ocurrio un error');
              }
            });
    }

    document.querySelector('.submit-button').onclick = formsubmit;

    document.querySelector('#form_contact').onclick = (e) => {
        e.preventDefault();
        document.querySelector('#form_contact').className = "animated zoomOut";
        document.querySelector('.form_content').classList.remove('fadeOutDown');
        setTimeout(() => {
            document.querySelector('#form_contact').style.display = 'none';
            document.querySelector('#close_contact').style.display = 'table';
            document.querySelector('#close_contact').className = 'animated zoomIn';
            document.querySelector('.form_content').style.display = 'block';
            document.querySelector('.form_content').classList.add('fadeInUp');
        }, 500);
    };

    document.querySelector('#close_contact').onclick = (e) => {
        e.preventDefault();
        document.querySelector('#close_contact').className = 'animated zoomOut';
        document.querySelector('.form_content').classList.remove('fadeInUp');
        document.querySelector('.form_content').classList.add('fadeOutDown');
        setTimeout(() => {
            document.querySelector('#form_contact').style.display = 'table';
            document.querySelector('#form_contact').className = 'animated zoomIn';
            document.querySelector('#close_contact').style.display = 'none';
            document.querySelector('.form_content').style.display = 'none';
        }, 500);
    };

};