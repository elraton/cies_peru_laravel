@extends('base')

@section('menu')
<ul class="menu">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link active">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('menu_mobile')
<ul class="menu_mobile">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link active">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('content')
<div class="container">
<div class="row">
    <div id="page_program" class="col-sm-12 page_program">
        <div class="row">
            <div class="col-sm-3">
                <img class="img-fluid" src="imgs/cecile.png">
                <h5 class="ponent_name text-center mt-3">DRA. CÉCILE MATHIEU</h5>
                <span class="cyan-text text-center">(DEL 15 AL 18 DE AGOSTO)</span>
                <div class="row">
                    <div class="col-sm-12 modulescont">
                        <a href="/program_thomas" class="modules">Módulo I</a>
                        <a href="/program_bricot" class="modules">Módulo II</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <h4 class="title">MÓDULO III: REFLEJOS ARCÁICOS</h4>
                <h6 class="cyan-text">LA CONTRIBUCIÓN DE LOS REFLEJOS ARQUÁICOS EN POSTUROLOGÍA CLÍNICA:
                        DE LA CUADRUPEDIA A LA VERTICALIDAD.</h6>

                <ul>
                    <li>La Posturología.
                        <ul>
                            <li>Generalidades.</li>
                            <li>Reseñas.</li>
                            <li>Criterios de normalidad.</li>
                        </ul>
                    </li>
                    <li>Los Reflejos Arcaicos
                        <ul>
                            <li>Presentación.</li>
                            <li>Antecedentes.</li>
                        </ul>
                    </li>
                    <li>Diseño del eje podo-visual
                        <ul>
                            <li>Introducción: "el eslabón perdido".</li>
                            <li>Reflexión sobre el desarrollo del niño.</li>
                            <li>¿Conceptos erróneos?.</li>
                            <li>Observaciones clínicas.</li>
                        </ul>
                    </li>
                    <li>Construcción del eje podo-visual de la cuadrupedia mediante la integración de los reflejos arcaicos
                        <ul>
                            <li>En intrauterino.</li>
                            <li>El nacimiento.</li>
                            <li>Encuentro con la gravedad.</li>
                            <li>Adaptación al mundo gravitacional.</li>
                            <li>La reptación hasta el pecho.</li>
                            <li>Reflejos del pie.
                                <ul>
                                    <li>El reflejo plantar y el reflejo de Babinski.</li>
                                    <li>Las funciones no reconocidas del reflejo de Babinski.</li>
                                    <li>Intrincaciones y especificidades.</li>
                                    <li>Observaciones detalladas.</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>Del eje podo-visual al eje visuo-podal.</li>
                    <li>Prevención.
                        <ul>
                            <li>Generalidades.</li>
                            <li>Especifidades:
                                <ul>
                                    <li>Posturales (Burllinger y otros).</li>
                                    <li>Cognitivas (la importancia de nombrar).</li>
                                    <li>Emocionales (la estructuración por la mirada, la relación con el otro, la conciencia del ser, la sensación cruda de sí mismo, la sensación que da sentido, la finalidad).</li>
                                    <li>¿Así como la tierra estaríamos perdiendo nuestro eje?.</li>
                                    <li>El mundo al revés.</li>
                                    <li>Desconexiones sensoriales, propioceptivas, adaptativas y energéticas.</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>Consecuencias y patologías por mal desarrollo
                        <ul>
                            <li>Posturales.</li>
                            <li>Cognitivas.</li>
                            <li>Emocionales.</li>
                        </ul>
                    </li>
                    <li>Nuevos criterios de evaluación para la evaluación postural.</li>
                    <li>Métodos terapéuticos.</li>
                    <li>Estudio detallado de los principales reflejos arcaicos para la evaluación del desarrollo postural:
                        <ul>
                            <li>Reflejo de miedo paralizante.</li>
                            <li>Reflejo de MORO.</li>
                            <li>Reflejo de BABKIN.</li>
                            <li>Reflejo de GRASPING.</li>
                            <li>Reflejo tónico laberíntico anterior.</li>
                            <li>Reflejo tónico asimétrico del cuello.</li>
                            <li>Reflejo tónico simétrico del cuello.</li>
                            <li>Reflejo espinal de GALANT.</li>
                            <li>Reflejo de tracción de las manos.</li>
                            <li>Reflejo tónico laberíntico posterior.</li>
                            <li>Reflejo espinal de PEREZ.</li>
                            <li>Reflejo de BABINSKI.</li>
                            <li>Reflejo de LANDAU superior e inferior.</li>
                            <li>Reflejo de protección de los tendones.</li>
                            <li>Reflejo anfibio.</li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
@endsection