@extends('base')

@section('menu')
<ul class="menu">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link active">Informes</a>
    </li>
</ul>
@endsection

@section('menu_mobile')
<ul class="menu_mobile">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link active">Informes</a>
    </li>
</ul>
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div id="page_contact" class="col-sm-12">
            <div class="row">
                <div class="col-sm-6">
                    <h4 class="title">INFORMES</h4>
                    <p class="info_text">Solicite información o realice su inscripción para la Formación Internacional en Posturología Clínica y Recalibración Postural por medio de una llamada o mensaje por Whatsapp al número:</p>
                    <a href="https://wa.me/51941571577" target="_blank" class="phone">
                        <img class="fb" src="imgs/whatsapp.png">
                        <span>941 571 577</span>
                    </a>
                    <p class="info_text">o envíenos un email a nuestro correo electrónico.</p>
                    <a href="mailto:informes@cies-peru.com" target="_blank" class="email">
                        <img src="imgs/envelope.png">
                        <span>informes@cies-peru.com</span>
                    </a>

                    <p class="info_text">Si deseas mas información en tu correo electrónico, haz click aqui y rellena este formulario.</p>
                    <a href="#" id="form_contact" class="animated">Ver Formulario</a>
                    <a href="#" id="close_contact" class="animated">Ocultar</a>

                    <div class="form_content animated">
                        <div class="form" id="form_info">
                            @csrf
                            <div class="content">
                                <h4 class="form_title">Solicitud de Información</h4>
                                <div class="form-row">
                                    <input type="text" id="form_name" name="name" placeholder="Nombre">
                                </div>
                                <div class="form-row2">
                                    <input type="email" id="form_email" name="email" placeholder="Email">
                                    <input type="phone" id="form_phone" name="phone" placeholder="Teléfono">
                                </div>
                                <div class="form-row">
                                    <input type="text" id="form_subject" name="subject" placeholder="Asunto">
                                </div>
                                <div class="form-row">
                                    <textarea placeholder="Mensaje" id="form_message" name="message" col="10"></textarea>
                                </div>
                                <div class="form-row justify-content-end">
                                    <button type="submit" class="submit-button">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="info_text">Visita nuestra página en Facebook y enterate de las últimas noticias.</p>

                    <a href="https://www.facebook.com/CiesPosturologiaPeru/" target="_blank" class="fb">
                        <img src="imgs/facebook_circle-512.png">
                    </a>

                    <p class="info_text">O también búscanos en Facebook y Google como: "Cies Posturología Perú"</p>
                </div>
                <div class="col-sm-6">
                    <div class="images_cont">
                        <img class="thomas_img" src="/imgs/thomas_2.jpg" alt="thomas">
                        <img class="bricot_img" src="/imgs/bricot_2.jpg" alt="bricot">
                        <img class="cecile_img" src="/imgs/cecile_2.jpg" alt="cecile">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="snackbar"></div>
</div>

@endsection