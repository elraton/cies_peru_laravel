@extends('base')

@section('menu')
<ul class="menu">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link active">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('menu_mobile')
<ul class="menu_mobile">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link active">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('content')
<div class="container">
<div class="row">
  <div id="page_ponents" class="col-sm-12">
    <h4 class="title">PLANA DOCENTE INTERNACIONAL</h4>
    <div class="row card-ponent">
      <div class="col-sm-3 ponent_photo">
        <img src="imgs/britcot.png">
      </div>
      <div class="col-sm-9 ponent_text">
        <h4 class="text_title">Dr. Bernard Bricot:</h4>
        <p>Cirujano Traumatólogo – Posturólogo – Auriculoterapéuta.</p>
        <p>Fundador del Método de Recalibración postural. Referente
            mundial y creador del método Reprogramación postural y de
            la escuela más importante del mundo en posturología. Doctor
            en cirugía ortopédica y especialista en posturología y estática.
            Es el Director del CIES-Marsella (Collège International d'Étude
            de la Statique). Colabora como profesor en el curso de
            posgrado de posturología y podoposturología de la
            Universitat de Barcelona.</p>
      </div>
    </div>
    <div class="row card-ponent">
      <div class="col-sm-3 ponent_photo">
        <img src="imgs/thomas.png">
      </div>
      <div class="col-sm-9 ponent_text">
        <h4 class="text_title">Dr. Thomas Solente:</h4>
        <p>Médico fisiatra – Osteópata – Posturólogo.</p>
        <p>Presidente del CIES Paraguay. Médico Cirujano diplomado de
            la Facultad de Medicina de la Universidad Nacional de
            Asunción – Paraguay, especializado en Francia. Es Presidente
            del CIES Paraguay - Collège International d`Étude de la
            Statique – y docente del Método BRICOT de Recalibración
            Postural Global.</p>
      </div>
    </div>
    <div class="row card-ponent">
      <div class="col-sm-3 ponent_photo">
        <img src="imgs/cecile.png">
      </div>
      <div class="col-sm-9 ponent_text">
        <h4 class="text_title">Lic. Cécile Mathieu:</h4>
        <p>Posturóloga – Especializada en Reflejos Arcaicos y en Energías
            Psico-emocionales.</p>
            <p>Osteopata Tradicional y Pediátrica Diplomada en Posturología
            por el CiES Francia y especializada en la Integración de los
            Reflejos Arcaicos. Es Autora de “La contribución del reflejo
            arcaico en la Posturología Clínica” y Directora del centro de
            Posturología de Ginebra Suiza.</p>
      </div>
    </div>
  </div>
</div>    
</div>
@endsection