@extends('base')

@section('menu')
<ul class="menu">
    <li class="item">
        <a href="/" class="link active">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('menu_mobile')
<ul class="menu_mobile">
    <li class="item">
        <a href="/" class="link active">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div id="page_home" class="col-sm-12">
      <div class="banner">
        <div class="floating-top">
          <img src="imgs/banner_top.png">
        </div>
        <div class="floating-left">
          <img src="imgs/banner_left.png">
        </div>
        <div class="floating-bottom">
          <img src="imgs/banner_bottom.png">
        </div>
        
        <div class="title">
          <h1>FORMACIÓN INTERNACIONAL</h1>
          <h6>EN POSTUROLOGÍA CLÍNICA Y RECALIBRACIÓN POSTURAL</h6>
        </div>

        <div class="subtitles">
          <h4>LIMA - PERÚ</h4>
          <h4>JUNIO, JULIO Y AGOSTO</h4>
          <h4>2019</h4>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection