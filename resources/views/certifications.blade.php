@extends('base')

@section('menu')
<ul class="menu">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link active">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('menu_mobile')
<ul class="menu_mobile">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link active">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div id="page_certifications" class="col-sm-12">
            <h4 class="title">BENEFICIOS</h4>
            <div class="row justify-content-between">
                <div class="col-sm-5 certificate">
                    <img src="/imgs/nacional.png">
                    <h4 class="head_title">CERTIFICACIÓN NACIONAL:</h4>
                    <p>Una vez que el alumno haya aprobado el módulo, obtendrá un certificado de la <b>Universidad Inca Garcilazo de la Vega</b> que acredite su participación en:</p>
                    <ul>
                        <li><p><b>Fundamentos de la Posturología.</b></p></li>
                        <li><p><b>Avances en Posturología.</b></p></li>
                        <li><p><b>Reflejos Arcáicos.</b></p></li>
                    </ul>
                    <p>El cual será firmado por el ponente de cada módulo, certificando la formación recibida.</p>
                </div>

                <div class="col-sm-5 certificate">
                    <img src="/imgs/internacional.png">
                    <h4 class="head_title">CERTIFICACIÓN INTERNACIONAL:</h4>
                    <p>Al aprobar los tres módulos, el alumno obtendrá un certificado de <b>CIES – FRANCIA</b> como:</p>
                    <ul>
                        <li><p>Fundamentos de la Posturología.</p></li>
                        <li><p>Avances en Posturología.</p></li>
                        <li><p>Reflejos Arcáicos.</p></li>
                    </ul>
                    <p>El cual será firmado por el ponente de cada módulo, certificando la formación recibida.</p>
                </div>

                <div class="col-sm-5 certificate">
                    <img src="/imgs/horario.png">
                    <h4 class="head_title">HORARIO DE CLASES:</h4>
                    <p>Módulos I y III:</p>
                    <ul>
                        <li><p>Jueves, viernes y sábado: De 08:30 a 19:00 Hrs.</p></li>
                        <li><p>Domingo: De 08:30 a 14:00 Hrs.</p></li>
                    </ul>
                    <p>Módulo II:</p>
                    <ul>
                        <li><p>Martes, miércoles y jueves: De 08:30 a 19:00 Hrs.</p></li>
                        <li><p>Viernes: De 08:30 a 14:00 Hrs.</p></li>
                    </ul>
                </div>

                <div class="col-sm-5 certificate">
                    <img src="/imgs/metodologia.png">
                    <h4 class="head_title">METODOLOGÍA::</h4>
                    <p>Todos los módulos incluyen desarrollo Teórico – Práctico.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection