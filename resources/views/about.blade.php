@extends('base')

@section('menu')
<ul class="menu">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link active">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('menu_mobile')
<ul class="menu_mobile">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link active">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
                <div id="page_about">
                        <h4 class="title">PRESENTACIÓN:</h4>
                        <h5>FORMACIÓN INTERNACIONAL EN POSTUROLOGÍA CLÍNICA Y RECALIBRACIÓN POSTURAL</h5>
                        <br>
                        <p><span class="blue-title">Fundamentos:</span> El Sistema Tónico Postural (STP) permite al cuerpo humano equilibrarse en la estática
                y en el movimiento, adaptándose a los estímulos del propio cuerpo y del medio externo. Las
                alteraciones del STP producen un abanico amplio de signos y síntomas, tanto del aparato locomotor
                como también aquellos relacionados con el equilibrio, las cefaleas, mareos, la cognición, el rendimiento
                deportivo, y otros que habitualmente son tratados de manera sintomática.</p>
                
                        <p><span class="blue-title">El objetivo de esta formación:</span> Es aprender a realizar un examen clínico postural completo y
                exhaustivo de estos sensores, de hacer correlaciones, y de encontrar el tratamiento que permite
                equilibrar el sistema postural y tratar la causa de los síntomas del paciente.</p>
                
                        <p><span class="blue-title">Nuestra formación es teórica y práctica: </span>Consiste en 3 módulos de 4 días cada uno. Carga
                horaria: 120 horas académicas.</p>
                
                        <p><span class="blue-title">Talleres en cada módulo: </span>Son parte esencial de la formación. Son dirigidos por el médico formador
                y los instructores capacitados. El alumno aprenderá a realizar un examen postural completo y a tratar
                al paciente.</p>
                
                
                        <h5>FORMACIÓN DIRIGIDA A:</h5>
                        <ul>
                            <li><p><b>Médicos clínicos y especialistas,</b> (traumatólogos ortopedistas, fisiatras, pediatras, oftalmólogos, reumatólogos, neurólogos, ginecólogos, especialistas en dolor)</p></li>
                            <li><p><b>Odontólogos</b></p></li>
                            <li><p><b>Fisioterapeutas y kinesiólogos</b></p></li>
                            <li><p><b>Podólogos</b></p></li>
                            <li><p><b>Ortoptistas</b></p></li>
                            <li><p><b>Osteópatas</b></p></li>
                            <li><p><b>Psicólogos</b> para el 3er Módulo (Reflejos Arcaicos)</p></li>
                        </ul>
                        <p><b><span class="blue-title">**</span> A todos los participantes se les brindará una formación de alta calidad y se integrará la
                            experiencia de los docentes en casos teóricos y prácticos aplicables.</b></p>
                
                    </div>
        </div>
    </div>
</div>
@endsection