@extends('base')

@section('menu')
<ul class="menu">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link active">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('menu_mobile')
<ul class="menu_mobile">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link active">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('content')
<div class="container">
<div class="row">
  <div id="page_program" class="col-sm-12 page_program">
    <div class="row">
      <div class="col-sm-3">
        <img class="img-fluid" src="imgs/thomas.png">
        <h5 class="ponent_name text-center mt-3">DR. THOMAS SOLENTE</h5>
        <span class="cyan-text text-center">(DEL 13 AL 16 DE JUNIO)</span>

        <div class="row">
          <div class="col-sm-12 modulescont">
            <a href="/program_bricot" class="modules">Módulo II</a>
            <a href="/program_cecile" class="modules">Módulo III</a>
          </div>
        </div>
      </div>
      <div class="col-sm-9">
        <h4 class="title">MÓDULO I: FUNDAMENTOS DE LA POSTUROLOGÍA</h4>
        <ul>
          <li>Estática normal y patológica en los 3 planos del espacio (frontal, sagital y rotatorio).</li>
          <li>Las consecuencias de los trastornos de la estática.
            <ul>
              <li>Pies asimétricos valgos y varos.</li>
              <li>Plan escapular anterior.</li>
              <li>Pies valgos y planos.</li>
            </ul>
          </li>
          <li>El doble péndulo fractal.</li>
          <li>La neurofisiología del sistema tónico postural.
            <ul>
              <li>La actividad tónica postural.</li>
              <li>Los diferentes elementos constituyentes.</li>
              <li>Los sensores principales.</li>
              <li>La computadora central.</li>
              <li>Las vías descendentes.</li>
              <li>El sistema de ejecución.</li>
            </ul>
          </li>
          <li>Las indicaciones para la reprogramación postural.</li>
          <li>Los principales sensores del cuerpo humano: nociones fundamentales.
            <ul>
              <li>El pie: los diferentes tipos y su evaluación.</li>
              <li>El ojo: bases neurofisiológicas, causas primitivas y segundarias, el examen del ojo, el examen de los anteojos, las bases del tratamiento de los trastornos musculares oculares.</li>
              <li>El aparato estomatognático: anatomía normal y armonía cráneo-facial, la integración del sistema estomatognático en el sistema postural, disfunciones cráneo-mandibulares, influencia de la ortodoncia sobre la postura, estudios complementarios, evaluación y bases del tratamiento.</li>
              <li>El oído: fisiología, mitos y realidad.</li>
            </ul>
          </li>
          <li>Los obstáculos a la recalibración postural.
            <ul>
              <li>Las desigualdades de longitud de los miembros inferiores: origen, examen físico y radiológica.</li>
              <li>Las cicatrices patológicas, fisiopatología e influencia sobre la postura, desajuste energético, desajuste metabólico, examen de las cicatrices, tratamiento.</li>
            </ul>
          </li>
          <li>Los bloqueos fuera de sistema: diferentes tipos de tratamientos.
            <ul>
              <li>Bloqueos de la primera costilla.</li>
              <li>Bloqueos del cóccix.</li>
              <li>Focos dentarios reactógenos.</li>
              <li>Microgalvanismos.</li>
            </ul>
          </li>
          <li>El examen postural completo: anamnesis, tipo de postura, evaluación de todos los sensores.</li>
          <li>Las consecuencias del trastorno postural: patologías del aparato locomotor y patologías generales.</li>
          <li>Bases para la recalibración postural.</li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>
@endsection