@extends('base')

@section('menu')
<ul class="menu">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link active">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('menu_mobile')
<ul class="menu_mobile">
    <li class="item">
        <a href="/" class="link">Inicio</a>
    </li>
    <li class="item">
        <a href="/about" class="link">Sobre el Evento</a>
    </li>
    <li class="item">
        <a href="/ponents" class="link">Expositores</a>
    </li>
    <li class="item">
        <a href="/program_thomas" class="link active">Módulos</a>
    </li>
    <li class="item">
        <a href="/certifications" class="link">Certificación</a>
    </li>
    <li class="item">
        <a href="/contact" class="link">Informes</a>
    </li>
</ul>
@endsection

@section('content')
<div class="container">
<div class="row">
    <div id="page_program" class="col-sm-12 page_program">
        <div class="row">
            <div class="col-sm-3">
                <img class="img-fluid" src="imgs/britcot.png">
                <h5 class="ponent_name text-center mt-3">DR. BERNARD BRICOT</h5>
                <span class="cyan-text text-center">(DEL 11 AL 14 DE JULIO)</span>
                <div class="row">
                    <div class="col-sm-12 modulescont">
                        <a href="/program_thomas" class="modules">Módulo I</a>
                        <a href="/program_cecile" class="modules">Módulo III</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <h4 class="title">MÓDULO II: AVANCES EN POSTUROLOGÍA</h4>

                <ul>
                    <li>Repaso de las nociones fundamentales.</li>
                    <li>Los obstáculos a la recalibración postural global.</li>
                    <li>Estudio en profundidad de los sensores: pie, ojo, aparato estomatognático, piel.</li>
                    <li>Neurofisiología del músculo y del sistema tónico postural.</li>
                    <li>Los criterios de normalidad.</li>
                    <li>Los test kinesiológicos.</li>
                    <li>Los perturbadores de la postura.</li>
                    <li>Los tipos de pies y sus correcciones en detalle.</li>
                    <li>Tratamiento detallado del sensor ocular.</li>
                    <li>El aparato estomatognático: estudio detallado y tratamiento en función de su influencia sobre la postura, criterios y resultados.</li>
                    <li>Ontogénesis, contrucción del sistema tónico postural, antecedentes patológicos de la infancia.</li>
                    <li>Influencia de los centros superiores: las heridas de la infancia.</li>
                    <li>La escoliosis: una enfermedad postural, los diferentes tipos.</li>
                    <li>Estudio de las patologías de origen postural:
                        <ul>
                            <li>Del aparato locomotor.</li>
                            <li>Cognitivas.</li>
                            <li>Inestabilidades y vertigos funcionales.</li>
                            <li>Cefaleas funcionales y migrañas.</li>
                            <li>Lesiones deportivas y rendimiento deportivo.</li>
                            <li>Psicológicas.</li>
                        </ul>
                    </li>
                    <li>Las correlaciones
                        <ul>
                            <li>Entre la postura y los distintos sensores.</li>
                            <li>Entre la postura y la lateralidad.</li>
                            <li>Entre el examen postural y las diferentes patologías posturales.</li>
                        </ul>
                    </li>
                    <li>La reprogramación postural: sus indicaciones, su alcance.</li>
                    <li>Evaluación del dolor.</li>
                    <li>Bases para la investigción: la estabilimetría.</li>
                    <li>Los tratamientos específicos y complementarios de todos los sensores y en función del terreno.</li>
                    <li>Síntesis diagnóstica y terapéutica.</li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
@endsection