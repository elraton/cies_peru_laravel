<!doctype html>
<html>
    <head>
        <title>CIES PERU | Escuela de entrenamiento de posturología clínica</title>
        <meta charset="UTF-8">
        <meta name="robots" content="index, follow"/>
        <meta name="googlebot" content="index, follow">
        <meta name="google" content="nositelinkssearchbox">
        <meta name="author" content="elratonmaton@gmail.com">
        <meta name="description"
            content="El cies es una asociación de posturología clínica creada en 
            Marsella en 1985 por el Doctor Bernard Bricot, Cies Peru se creó para formar 
            nuevos talentos en posturología clínica y afines" />
        <meta http-equiv="Content-Language" content="es"/>
        <meta name="distribution" content="global"/>
        <meta name="revisit-after" content="7 days">
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <link rel="shortcut icon" href="/imgs/logo-cies.png">
        <link rel="canonical" href="https://cies-peru.com"/>
        <link href="css/app.css?v=1.2" rel="stylesheet">
    </head>
    <body>
        <div class="header">
            <div class="spacer"></div>
            <div class="content">
                <a class="logo_header" href="#"><img src="imgs/logo-cies.png"></a>
                <div class="menu_cont">
                    @section('menu')
                    @show
                </div>
                <a class="menu_mobile_button">
                    <img class="menu_icon" src="imgs/menu.png">
                </a>
                <div class="menu_mobile_cont close">
                    @section('menu_mobile')
                    @show
                    <a class="close_menu_button"><img src="imgs/closemenu.png"></a>
                </div>
            </div>
        </div>
        @section('content')
        @show
        <script src="js/app.js?v=1.1"></script>
  </body>
</html>