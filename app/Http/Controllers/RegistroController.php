<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\ClientEmail;
use App\Mail\AdminEmail;

use Mail; 

class RegistroController extends Controller
{
    public function insert(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $subject = $request->subject;
        $message = $request->message;

        if ( strlen($name) > 30) {
            return response()->json([ 'data' => 'fail' ], 500);
        }

        if ( strlen($email) > 50) {
            return response()->json([ 'data' => 'fail' ], 500);
        }

        if ( strlen($phone) > 15) {
            return response()->json([ 'data' => 'fail' ], 500);
        }

        if ( strlen($subject) > 100) {
            return response()->json([ 'data' => 'fail' ], 500);
        }
        if ( strlen($message) > 500) {
            return response()->json([ 'data' => 'fail' ], 500);
        }

        $register = DB::table('registers')->where('email', $email)->first();
        if ($register) {
            return response()->json([
                'data' => 'fail',
            ], 500);
        }

        DB::table('registers')->insert(
            [
                'name'  => $name, 
                'email' => $email,
                'phone' => $phone,
                'subject' => $subject,
                'message' => $message
            ]
        );

        $data = ['message' => 'En breve un representante se comunicará contigo'];
        Mail::to($email)->send(new ClientEmail($data));

        $data = [
            'message' => 'Se registro un nuevo participante',
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'subject' => $subject,
            'message2' => $message
        ];
        Mail::to('registro@cies-peru.com')->send(new AdminEmail($data));
        // Mail::to('elratonmaton@gmail.com')->send(new AdminEmail($data));

        return response()->json([
            'data' => 'ok',
        ], 200);
    }

    //
}