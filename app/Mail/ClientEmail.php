<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClientEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'registro@cies-peru.com';
        $subject = 'Gracias por registrarte!';
        $name = 'Registro CIES PERÚ';
        
        return $this->view('emails.clientemail')
                    ->subject($subject)
                    ->with([ 'clientmessage' => $this->data['message'] ]);
    }
}